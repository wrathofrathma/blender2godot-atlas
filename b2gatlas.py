#!/usr/bin/python

import argparse
from PIL import Image
import os
import sys
import glob
import math
import json

# Rotation Legend
# 0  = Don't Touch
# 1 = Rotate 90 clockwise
# 2 = Rotate 180
# 3 = Rotate 90 counterclockwise
# 0 0 0
# 1 1 0
# 1 2 3

def fix_rotation(image: Image.Image) -> Image.Image:
    """Fixes the rotation of the texture"""
    r_map = (
        [ 0, 0, 0 ],
        [ 1, 1, 0 ],
        [ 1, 2, 3 ]
    )

    tile_size = int(image.size[0] / 3.0)

    for row in range(len(r_map)):
        for col in range(len(r_map[row])):
            left = col * tile_size
            upper = row * tile_size
            right = (col+1) * tile_size
            lower = (row+1) * tile_size
            region_box = (left, upper, right, lower)

            region = image.crop(region_box)
            if(r_map[row][col]==1):
                region = region.transpose(Image.ROTATE_270)
            if(r_map[row][col]==2):
                region = region.transpose(Image.ROTATE_180)
            if(r_map[row][col]==3):
                region = region.transpose(Image.ROTATE_270)
            image.paste(region, region_box)
    return image

# Godot cube tile order
# Left, Right, Bottom, Top, Back, Front
#
# b2g face map
# Top   0    0
# Right Back 0
# Front Left Bottom

def split_regions(im: Image.Image) -> tuple:
    """Splits the regions of a blender image to 6 individual regions and returns them\
    in the order we're going to implement them in godot"""

    tile_size = int(im.size[0] / 3.0)

    # left up right down
    top = im.crop((0, 0, tile_size, tile_size))
    right = im.crop((0, tile_size, tile_size, tile_size * 2))
    front = im.crop((0, tile_size*2, tile_size, tile_size * 3))
    back = im.crop((tile_size, tile_size, tile_size*2, tile_size*2))
    left = im.crop((tile_size, tile_size*2, tile_size*2, tile_size*3))
    bottom = im.crop((tile_size*2, tile_size*2, tile_size*3, tile_size * 3))

    return(left,right,bottom,top,back,front)

def create_atlas(args: dict):
    directory = args["directory"]
    n_ext = "_n.png" if args["normals"] is None else args["normals"] # Normal extension
    root = "textures" if args["out"] is None else args["out"] # Output root
    asc = not args["desc"] # Ascending?
    all_rot = args["all"] # Generate all rotations?
    fix_rot = not args["no_rotation"]
    margins = 0 if args["margin"] is None else args["margin"]
    atlas_path = "atlas.json" if args["atlas"] is None else args["atlas"]

    assert margins == 0, "Implement margins, for both blender exported and our exported. Don't be lazy"

    # Check directory exists
    if not os.path.exists(directory):
        print("Directory doesn't exist")
        return
    if not os.path.isdir(directory):
        print("Path is not a directory")
        return

    texture_list = os.listdir(directory)
    n_textures = len(texture_list)

    if(not asc):
        texture_list.reverse()

    # We expect all images to be the same size, so let's prefetch the first one that exists and grab the dimensions
    prefetch_path = glob.glob(directory + "/*/*.png")[0]
    prefetch_img = Image.open(prefetch_path)
    size = prefetch_img.size
    tile_size = int(size[0] / 3.0)
    prefetch_img.close()

    # Create image based on size and number of textures
    # Godot VoxelTools by Zylann's cubes use an atlas that's expected to either be 1xn nx1 or nxn dimensions.
    # I'm just going to go with a scheme that puts it in nxn dimensions and stores atlas data in a json
    diffuse = None
    normal = None
    n = 0
    assert not all_rot, "Implement me bitch"
    if(all_rot):
        n_tiles = n_textures * 6 * 6
        n = math.ceil(math.sqrt(n_tiles))
        res = (n * tile_size, n * tile_size)
        diffuse = Image.new("RGBA", res, color=0)
        normal = Image.new("RGBA", res, color=0)
    else:
        n_tiles = n_textures * 6
        n = math.ceil(math.sqrt(n_tiles))
        res = (n * tile_size, n * tile_size)
        diffuse = Image.new("RGBA", res, color=0)
        normal = Image.new("RGBA", res, color=0)

    atlas = { "size": n, "voxels": {}} # Atlas will be dumped later into a json. This will hold the tile mappings to cube id
    n_stored = 0
    for i in range(len(texture_list)):
        # We expect the image to be under the same name as the directory
        base = texture_list[i]
        base_path = "{top_dir}/{base}/{base}.png".format(top_dir = directory, base=base)
        normal_path = "{top_dir}/{base}/{base}{ext}".format(top_dir = directory, base=base, ext=n_ext)

        atlas["voxels"][base] = []

        if(os.path.exists(base_path)):
            # add to diffuse
            im = Image.open(base_path)
            assert im.size == size, "Texture sizes not consistent."

            if(fix_rot):
                im = fix_rotation(im)

            regions = split_regions(im)
            for j in range(6):
                # 1 1 1 1
                # 1 1 0 0
                # 0 0 0 0
                # 0 0 0 0
                # If we fill each row, we can calc x
                # stored = 6
                # n = 4
                # x = stored % n
                # y = floor(stored / n)
                x = n_stored % n
                y = math.floor(n_stored / n)

                top = y * tile_size
                bottom = (y+1) * tile_size
                left = x * tile_size
                right = (x+1) * tile_size

                box = (left, top, right, bottom)
                diffuse.paste(regions[j], box)
                # Store in atlas
                atlas["voxels"][base] += [(x,y)]

                n_stored+=1
            im.close()


        if(os.path.exists(normal_path)):
            # add to diffuse
            im = Image.open(normal_path)
            assert im.size == size, "Texture sizes not consistent."

            if(fix_rot):
                im = fix_rotation(im)

            regions = split_regions(im)
            for j in range(6):
                # 1 1 1 1
                # 1 1 0 0
                # 0 0 0 0
                # 0 0 0 0
                # If we fill each row, we can calc x
                # stored = 6
                # n = 4
                # x = stored % n
                # y = floor(stored / n)
                x = n_stored % n
                y = math.floor(n_stored / n)

                top = y * tile_size
                bottom = (y+1) * tile_size
                left = x * tile_size
                right = (x+1) * tile_size

                box = (left, top, right, bottom)
                normal.paste(regions[j], box)
                # Store in atlas

                n_stored+=1
            im.close()


    json.dump(atlas, open(atlas_path, "w"))
    diffuse.save(root + ".png")
    normal.save(root + n_ext)


if __name__=="__main__":
    parser = argparse.ArgumentParser(description="Texture atlas creator for blender => godot workflow")
    parser.add_argument("--desc", action="store_true", help="Generate descending list(Lowest at bottom). Default is ascending")
    parser.add_argument("--normals", help="Specify normals file extension, default is _n.png")
    parser.add_argument("--all", action="store_true", help="Generates rotation texture for each possible orientation(Very wasteful)")
    parser.add_argument("--no-rotation", action="store_true", help="Don't attempt to fix the rotation of textures from blender's rotated UV mapping")
    parser.add_argument("--out", metavar="output", type=str, help="Output file root. Default textures.png textures_n.png. Output will be root.png & root_n.png")
    parser.add_argument("--margin", type=int, help="Blender island margins")
    parser.add_argument("--atlas", type=str, help="Path to the atlas json")
    parser.add_argument("directory", metavar='directory', type=str, help="Root directory of textures")

    args = parser.parse_args()
    args = vars(args)


    create_atlas(args)
